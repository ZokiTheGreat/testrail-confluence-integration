package com.bssys.testrail.confluence.integration.macro;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Map;

@ExportAsService
@Component
public class Helloworld implements Macro {

    private static final Logger log = LoggerFactory.getLogger(Helloworld.class);

    private PageBuilderService pageBuilderService;

    @Inject
    public Helloworld(@ComponentImport PageBuilderService pageBuilderService) {
        this.pageBuilderService = pageBuilderService;
    }


    public String execute(Map<String, String> map, String s, ConversionContext conversionContext) throws MacroExecutionException {

        pageBuilderService.assembler().resources().requireWebResource("com.bssys.testrail.confluence.testrail-confluence-integration:testrail-confluence-integration-resources");

        log.info("HW marco executed");

        String output = "<div class =\"Helloworld\">";
        output = output + "<div class = \"" + map.get("Color") + "\">";


        if (map.get("Name") != null) {
            output = output + ("<h1>Привет, " + map.get("Name") + "!</h1>");
        } else {
            output = output + "<h1>Здравствуй, МИР!<h1>";
        }

        output = output + "</div>" + "</div>";

        return output;
    }

    public BodyType getBodyType() { return BodyType.NONE; }

    public OutputType getOutputType() { return OutputType.BLOCK; }
}
