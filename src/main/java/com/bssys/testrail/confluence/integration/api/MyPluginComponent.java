package com.bssys.testrail.confluence.integration.api;

public interface MyPluginComponent
{
    String getName();
}