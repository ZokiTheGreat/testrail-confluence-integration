package com.bssys.testrail.confluence.listeners;

import com.atlassian.confluence.content.render.xhtml.transformers.Transformer;
import com.atlassian.confluence.event.events.content.page.PageUpdateEvent;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.util.LabelUtil;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.vcache.VCacheFactory;
import com.bssys.testrail.confluence.pages.ConfluencePageController;
import com.bssys.testrail.confluence.pages.PageChangesHandler;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.stereotype.Component;
/*import org.slf4j.Logger;
import org.slf4j.LoggerFactory;*/
import org.apache.log4j.Logger;


import javax.inject.Inject;

@Component
public class PageUpdateListener implements DisposableBean {

    private static final Logger log = Logger.getLogger(PageUpdateListener.class);

    private final EventPublisher eventPublisher;
    private final LabelManager labelManager;
    private final PageManager pageManager;
    private ConfluencePageController confluencePageController;

    @Inject
    public PageUpdateListener(@ComponentImport EventPublisher eventPublisher, @ComponentImport LabelManager labelManager, @ComponentImport PageManager pageManager,
                              @ComponentImport Transformer transformer, @ComponentImport VCacheFactory cacheFactory,
                              @ComponentImport LocaleManager localeManager){
        this.eventPublisher = eventPublisher;
        this.labelManager = labelManager;
        this.pageManager = pageManager;
/*        ArrayList<DiffPostProcessor> postProcessors = new ArrayList<DiffPostProcessor>();
        postProcessors.add(diffPostProcessor);*/
        this.confluencePageController = new ConfluencePageController(pageManager, transformer,/* postProcessors, */cacheFactory, localeManager);
        eventPublisher.register(this);
    }

    @EventListener
    public void onPageUpdateEvent(PageUpdateEvent event) {
        String diff;
        Page page = event.getPage();
        int prevVersion = page.getPreviousVersion();
        Page origPage = (Page) pageManager.getPageByVersion(page, prevVersion);
        confluencePageController.setPage(page);
        confluencePageController.setPrevVersionPage(origPage);

        LabelUtil.addLabel("test-update", labelManager, page);

/*        log.info("URAAAAAH!!!  page updated");
        log.warn(event);
        log.debug("asdasdasdasd DEBUG");*/
//        confluencePageController.diffWithDiffer();
        confluencePageController.diffPages();
        diff = confluencePageController.getDiff();
        PageChangesHandler pageChangesHandler = new PageChangesHandler(diff);

        log.warn("***********************************PageUpdateListener*************************************\n");
        log.warn("Page title:" + page.getTitle());
        log.warn("Changed page title though diff:" + diff);
        log.warn("Changed entities:" + pageChangesHandler.getChangedEntities());
//        log.warn("Changed entities parents:" + pageChangesHandler.getChangeParent(null));
        log.warn("Changed processed entities:" + confluencePageController.getElementChangeTypeMap());
//        log.warn("Original page title though diff:" + prevVersionResult);
        log.warn("*****************************************************************************************\n");
//        log.error("ERRRRROOOOOR");
//        log.trace("TRAAACE");
    }

    @Override
    public void destroy() throws Exception {
        eventPublisher.unregister(this);
    }
}