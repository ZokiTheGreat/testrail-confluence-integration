package com.bssys.testrail.confluence.pages;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.Map;

public class PageChangesHandler {

    private String pageHtml;
    private Document document;
    private Elements nodesOfChanges;
    private Map<Element, String> changedElementEntityMap;

    public PageChangesHandler(String pageHtml) {
        this.pageHtml = pageHtml;
        this.document = Jsoup.parseBodyFragment(pageHtml);
    }

    public Elements getChangedEntities(){
        this.nodesOfChanges = document.select("span[class^=\"diff-html\"]");
        return nodesOfChanges;
    }

/*    public Element getChangeParent(Element change, String type){
        Element result;
        switch (type){
            case "text":
                result = change.parent();
                break;
            default:
                result = change.parent().parent();
        }
        return result;
    }

    public void setChangedElementEntityMap(Map<Element, String> elementChangeTypeMap) {
        for (Element chElem :
                elementChangeTypeMap.keySet()) {
            switch (elementChangeTypeMap.get(chElem)){
                case "requirement":
                    this.changedElementEntityMap.put(chElem, null);
                    break;
                case "testcase":
                    break;
                case "text":
                    break;
            }
        }
        this.changedElementEntityMap = elementChangeTypeMap;
    }*/
}