package com.bssys.testrail.confluence.pages;

import com.bssys.testrail.confluence.integration.api.testrail.APIClient;
import com.bssys.testrail.confluence.integration.api.testrail.APIException;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestCaseHandler {
    private static final Logger log = Logger.getLogger(TestCaseHandler.class);

    private final APIClient testRailApiClient;
    private JSONObject jsonTestCase;
    private Properties properties;

    public TestCaseHandler() {
        String baseUrl="https://testrail.bssys.com";
        String login="N.Zyrianov@bssys.com";
        String password="ASD89gsduye33";
        /*this.properties = new Properties();
        String baseUrl = properties.getProperty("testrail.baseurl");
        String login = properties.getProperty("testrail.login");
        String password = properties.getProperty("testrail.password");*/
        this.testRailApiClient = new APIClient(baseUrl);
        this.testRailApiClient.setUser(login);
        this.testRailApiClient.setPassword(password);
    }

    //Just of HTML-tables
    public static Set findTcInTable(Element change){
        Set result = new HashSet();
        Element currentNode;
        Elements tcLinks = null;
        Element chNode = change.parent();
        while (chNode.nextElementSibling() != null){
            currentNode = chNode.nextElementSibling();
            tcLinks = currentNode.select("a[href*=\"testrail\"");
            if (tcLinks != null)
                break;
            chNode = chNode.nextElementSibling();
        }
        for (Element link :
                tcLinks) {
            result.add(link.attr("href"));
//            result += link.attr("href") + ",";
        }
        return result;
    }

    public Map<String, String> addReferenceToTestCase(Map<String,String> testCaseMap){
        if (testCaseMap.get("refs") != null){

        }
        return testCaseMap;
    }

    public Map<String,String> getTestCase(String testCaseId){
        JSONObject tc = null;
        try {
            tc = (JSONObject) testRailApiClient.sendGet("get_case/" + testCaseId);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (APIException e) {
            e.printStackTrace();
        }
        this.jsonTestCase = tc;
        return tc;
    }

    public void executeTRFunc(String testCasesIds, String requirement){
        JSONObject c = null;
        try {
            c = (JSONObject) testRailApiClient.sendGet("get_case/" + testCasesIds);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (APIException e) {
            e.printStackTrace();
        }
        String refs = (String) c.get("refs");
        if (refs == null)
            refs = requirement;
        else
            refs += "," + requirement;

        log.warn("TITLE OF TC: "+ c.get("title"));
        HashMap<String, String> tcData = new HashMap<>();
        tcData.put("custom_tc_state", "3");
        tcData.put("refs", refs);
        try {
            testRailApiClient.sendPost("update_case/" + testCasesIds, tcData);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (APIException e) {
            e.printStackTrace();
        }
    }

    public static String getTcIdFromLink(String tcLink){
        String regex = "(/)(\\d*$)";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(tcLink);

        if (matcher.find())
            return matcher.group();
        else
            return null;
    }

    public APIClient getTestRailApiClient() {
        return testRailApiClient;
    }
}
