package com.bssys.testrail.confluence.pages;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.HashSet;
import java.util.Set;

public class RequirementHandler {

    private String reqID;
    private Element reqNode;

    //Just of HTML-tables
    public String findReqInTable(Element change){
        String result = null;
        Element currentNode;
        Element resultNode = null;
        Elements reqNodes;
        Element chNode = change.parent();
        while (chNode.previousElementSibling() != null){
            currentNode = chNode.previousElementSibling();
            reqNodes = currentNode.select("structured-macro");
            if (reqNodes != null) {
                for (Element reqNode :
                        reqNodes) {
                    if (reqNode.attr("name").equals("requirement") && this.isReqTypeDefinition(reqNode)) {
                        resultNode = reqNode;
                        break;
                    }
                }
                break;
            }
            chNode = chNode.previousElementSibling();
        }
        if (resultNode != null) {
            this.reqNode = resultNode;
            result = getReqID();
        }
        return result;
    }

    public boolean isReqTypeDefinition(Element reqNode){
        boolean result = false;
        if (this.getReqType(reqNode).equals("DEFINITION"))
            result = true;
        return result;
    }

    public String getReqType(Element reqNode){
        String reqType = null;
        Elements reqParamTypes = reqNode.select("parameter[name=\"type\"]");
        if (reqParamTypes != null)
            reqType = reqParamTypes.get(0).text();
        return reqType;
    }

    public String getReqID() {
        if (this.reqNode != null){
            Element paramKey = this.reqNode.select("parameter[name=\"key\"]").first();
            return paramKey.text();
        }
        else
            return this.reqID;
    }
}