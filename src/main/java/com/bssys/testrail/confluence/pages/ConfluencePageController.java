package com.bssys.testrail.confluence.pages;

import com.atlassian.confluence.content.render.xhtml.transformers.Transformer;
import com.atlassian.confluence.diff.DaisyHtmlDiffer;
import com.atlassian.confluence.diff.Differ;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.pages.actions.DiffPagesAction;
import com.atlassian.vcache.VCacheFactory;
import com.bssys.testrail.confluence.integration.api.testrail.APIException;
import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.apache.log4j.Logger;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


public class ConfluencePageController {

    private static final Logger log = Logger.getLogger(ConfluencePageController.class);

    private static Page page;
    private static Page prevVersionPage;
    private final Differ differ;
    private final TestCaseHandler testCaseHandler;
    private final RequirementHandler requirementHandler;
    private String diffResult;
    private Map <Element, String> elementChangeTypeMap;
    private Map<Element, String> elementChangedEntityMap;
    private Elements pageChanges;

    private DiffPagesAction diffPagesAction;
    private PageChangesHandler pageChangesHandler;

    public ConfluencePageController(PageManager pageManager, Transformer transformer, VCacheFactory cacheFactory,
                                    LocaleManager localeManager) {
        this.testCaseHandler = new TestCaseHandler();
        this.requirementHandler = new RequirementHandler();
        this.differ = new DaisyHtmlDiffer(transformer,null,cacheFactory,localeManager);
        this.diffPagesAction = new DiffPagesAction();
        this.diffPagesAction.setPageManager(pageManager);
        this.diffPagesAction.setHtmlDiffer(this.differ);
    }

    public void diffPages() {
        this.diffWithDiffer();
        this.pageChangesHandler = new PageChangesHandler(this.diffResult);
        this.processChanges();
/*        String diffExecutionResult;
        try {
            diffExecutionResult = this.diffPagesAction.execute();
            if (diffExecutionResult == "success")
                this.diffResult = this.diffPagesAction.getDiff();
            else if (diffExecutionResult == "error")
                this.diffResult = "COMPARING FAILED, AARRGH!!!";
        } catch (Exception e) {
            log.error("****************************MyConfPageException*****************************************\n" +
                    "|||PAGE: " + this.getPage().getUrlPath() + "|||PREVVERSIONPAGE: " + this.getPrevVersionPage().getUrlPath() +
                      "|||DIFFPAGESACTION getOriginalPage:" + this.diffPagesAction.getOriginalPage().getUrlPath() + "|||" + this.diffPagesAction.getOriginalId() +
                    "*****************************************************************************************\n");
            e.printStackTrace();
        }*/
    }

    public void diffWithDiffer(){
//        this.diffResult = this.differ.diff(page, prevVersionPage);
        this.diffResult = this.differ.diff(prevVersionPage, page);

    }

    private void processChanges(){
        log.warn("***********************************ProcessChanges***************************************\n");
        Elements changes = pageChangesHandler.getChangedEntities();
        Map <Element, String> elementTypeMap = new HashMap<>();
        String testCasesIds = "";
        String requirement = null;
        for (Element change :
                changes) {
            if (change.hasText()){
                switch (change.parent().tagName()){
                    case "parameter":
                        if (change.parent().parent().tagName().equals("structured-macro"))
                            elementTypeMap.put(change, "requirement");
                        break;
                    case "span":
                        if (change.parent().parent().tagName().equals("a"))
                            elementTypeMap.put(change, "testcase");
                        break;
                    default:
                        elementTypeMap.put(change, "text");
                        Set<String> tcLinks = testCaseHandler.findTcInTable(change);
                        for (String tcLink:
                             tcLinks) {
                            testCasesIds += testCaseHandler.getTcIdFromLink(tcLink) + ",";
                        }
                        testCasesIds = testCasesIds.substring(0, testCasesIds.length() - 1);
                        requirement = requirementHandler.findReqInTable(change);

                        log.warn("TEST CASES: " + tcLinks);
                        log.warn("TC ID: " + testCasesIds);
                        log.warn("REQUIREMENT ID: " + requirement);
                }
            }
        }
        this.elementChangeTypeMap = elementTypeMap;

        testCaseHandler.executeTRFunc(testCasesIds, requirement); //Временно
    }

    public String[] getChangedTestCases(Elements changes){
        String[] testCases = new String[0];
        return testCases;
    }

    public String getPageContent(AbstractPage page){
        return page.getBodyContent().getBody();
    }

    public Document getPageHtml(AbstractPage page){
        String pageContent = getPageContent(page);
        Document document = Jsoup.parse(pageContent);
        return document;
    }

    public Page getPage() {
        return page;
    }

    public Page getPrevVersionPage() {
        return prevVersionPage;
    }

    public Map<Element, String> getElementChangeTypeMap() {
        return elementChangeTypeMap;
    }

    public Map<Element, String> getElementChangedEntityMap() {
        return elementChangedEntityMap;
    }

    public Elements getPageChanges() {
        return pageChanges;
    }

    public String getDiff(){
        return this.diffResult;
    }

    public void setPage(Page page) {
        this.page = page;
        this.diffPagesAction.setPage(page);
    }

    public void setPrevVersionPage(Page prevVersionPage) {
        log.warn("**************************MyConfPageSetPrevVersionPage**********************************\n" +
                 "|||PAGE: " + prevVersionPage.getUrlPath() + "|||ID: " + prevVersionPage.getId());
        this.prevVersionPage = prevVersionPage;
        this.diffPagesAction.setOriginalId(prevVersionPage.getId());
        log.warn("AFTER SETTING ID: " + this.diffPagesAction.getOriginalId());
    }
}
